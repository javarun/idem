import pathlib
import shutil
import subprocess
import sys
import tempfile

import msgpack

PASS_STATE = """
passing_state:
  test.present:
    - new_state:
       a: b
"""

CHANGED_STATE = """
passing_state:
  test.present:
    - new_state:
       a: c
"""

FAILED_STATE = """
passing_state:
  test.present:
    - result: false
    - new_state:
       a: c
"""

FAILED_STATE_FORCE_SAVE = """
passing_state:
  test.present:
    - result: false
    - force_save: true
    - new_state:
       a: c
"""

IGNORE_CHANGES_STATE_CREATE = """
passing_state:
  test.update:
    - name: my-state
    - result: true
    - param_1: {a: b}
    - param_2:
       a: {b: c}
       b: c
    - ignore_changes:
      - param_2
"""

IGNORE_CHANGES_STATE_UPDATE = """
passing_state:
  test.update:
    - name: my-state
    - result: true
    - param_1: {a: c}
    - param_2:
       a: {b: d}
       b: d
    - ignore_changes:
      - param_1
      - param_2
"""

IGNORE_CHANGES_STATE_UPDATE_2 = """
passing_state:
  test.update:
    - name: my-state
    - result: true
    - param_1: {a: b}
    - param_2:
       a: {b: d}
       b: d
    - ignore_changes:
      - param_2:a:b
"""


def test_ctx_test(runpy):
    """
    When the `--test` flag is given on the cli, ESM should not be updated
    """
    cache_dir = pathlib.Path(tempfile.mkdtemp())
    esm_cache = cache_dir / "cache" / "esm" / "local" / "test.msgpack"
    assert not esm_cache.exists()

    try:
        # Run the state to create an esm cache
        with tempfile.NamedTemporaryFile("w+", suffix=".sls", delete=True) as fh:
            fh.write(PASS_STATE)
            fh.flush()

            cmd = [
                sys.executable,
                str(runpy),
                "state",
                fh.name,
                "--log-level=debug",
                f"--cache-dir={cache_dir / 'cache'}",
                f"--root-dir={cache_dir}",
                "--log-level=debug",
                "--run-name=test",
            ]
            ret = subprocess.run(cmd, capture_output=True, encoding="utf-8")

        assert ret.returncode == 0, ret.stderr
        assert esm_cache.exists()

        with esm_cache.open("rb") as fh:
            data = msgpack.load(fh)
            assert data == {"test_|-passing_state_|-passing_state_|-": {"a": "b"}}

        # Change the state and run with the test flag,  nothing should change
        with tempfile.NamedTemporaryFile("w+", suffix=".sls", delete=True) as fh:
            fh.write(CHANGED_STATE)
            fh.flush()

            cmd = [
                sys.executable,
                str(runpy),
                "state",
                fh.name,
                "--log-level=debug",
                "--test",
                f"--cache-dir={cache_dir / 'cache'}",
                f"--root-dir={cache_dir}",
                "--log-level=debug",
                "--run-name=test",
            ]
            ret = subprocess.run(cmd, capture_output=True, encoding="utf-8")

        assert ret.returncode == 0, ret.stderr
        assert esm_cache.exists()

        with esm_cache.open("rb") as fh:
            data = msgpack.load(fh)
            assert data == {"test_|-passing_state_|-passing_state_|-": {"a": "b"}}
    finally:
        shutil.rmtree(cache_dir, ignore_errors=True)
        assert not cache_dir.exists(), f"Could not remove cache dir: {cache_dir}"


def test_fail(runpy):
    """
    When a state fails during update, it should not update ESM
    """
    cache_dir = pathlib.Path(tempfile.mkdtemp())
    esm_cache = cache_dir / "cache" / "esm" / "local" / "test.msgpack"
    assert not esm_cache.exists()

    try:
        # Run the state to create an esm cache
        with tempfile.NamedTemporaryFile("w+", suffix=".sls", delete=True) as fh:
            fh.write(PASS_STATE)
            fh.flush()

            cmd = [
                sys.executable,
                str(runpy),
                "state",
                fh.name,
                "--log-level=debug",
                f"--cache-dir={cache_dir / 'cache'}",
                f"--root-dir={cache_dir}",
                "--log-level=debug",
                "--run-name=test",
            ]
            ret = subprocess.run(cmd, capture_output=True, encoding="utf-8")

        assert ret.returncode == 0, ret.stderr
        assert esm_cache.exists()

        with esm_cache.open("rb") as fh:
            data = msgpack.load(fh)
            assert data == {"test_|-passing_state_|-passing_state_|-": {"a": "b"}}

        # Change the state so that it fails, nothing should change
        with tempfile.NamedTemporaryFile("w+", suffix=".sls", delete=True) as fh:
            fh.write(FAILED_STATE)
            fh.flush()

            cmd = [
                sys.executable,
                str(runpy),
                "state",
                fh.name,
                "--log-level=debug",
                f"--cache-dir={cache_dir / 'cache'}",
                f"--root-dir={cache_dir}",
                "--log-level=debug",
                "--run-name=test",
            ]
            ret = subprocess.run(cmd, capture_output=True, encoding="utf-8")

        assert ret.returncode == 0, ret.stderr
        assert esm_cache.exists()

        with esm_cache.open("rb") as fh:
            data = msgpack.load(fh)
            assert data == {"test_|-passing_state_|-passing_state_|-": {"a": "b"}}

        # Change the state with a failed state but with force_save to be True, this should result an esm update
        with tempfile.NamedTemporaryFile("w+", suffix=".sls", delete=True) as fh:
            fh.write(FAILED_STATE_FORCE_SAVE)
            fh.flush()

            cmd = [
                sys.executable,
                str(runpy),
                "state",
                fh.name,
                "--log-level=debug",
                f"--cache-dir={cache_dir / 'cache'}",
                f"--root-dir={cache_dir}",
                "--log-level=debug",
                "--run-name=test",
            ]
            ret = subprocess.run(cmd, capture_output=True, encoding="utf-8")

        assert ret.returncode == 0, ret.stderr
        assert esm_cache.exists()

        with esm_cache.open("rb") as fh:
            data = msgpack.load(fh)
            assert data == {"test_|-passing_state_|-passing_state_|-": {"a": "c"}}
    finally:
        shutil.rmtree(cache_dir, ignore_errors=True)
        assert not cache_dir.exists(), f"Could not remove cache dir: {cache_dir}"


def test_fail_on_create(runpy):
    """
    When a state fails during creation, it should update ESM if new_state is not empty
    """
    cache_dir = pathlib.Path(tempfile.mkdtemp())
    esm_cache = cache_dir / "cache" / "esm" / "local" / "test.msgpack"
    assert not esm_cache.exists()

    try:
        # Run the state to create an esm cache
        with tempfile.NamedTemporaryFile("w+", suffix=".sls", delete=True) as fh:
            fh.write(FAILED_STATE)
            fh.flush()

            cmd = [
                sys.executable,
                str(runpy),
                "state",
                fh.name,
                "--log-level=debug",
                f"--cache-dir={cache_dir / 'cache'}",
                f"--root-dir={cache_dir}",
                "--log-level=debug",
                "--run-name=test",
            ]
            ret = subprocess.run(cmd, capture_output=True, encoding="utf-8")

        assert ret.returncode == 0, ret.stderr
        assert esm_cache.exists()

        with esm_cache.open("rb") as fh:
            data = msgpack.load(fh)
            assert data == {"test_|-passing_state_|-passing_state_|-": {"a": "c"}}
    finally:
        shutil.rmtree(cache_dir, ignore_errors=True)
        assert not cache_dir.exists(), f"Could not remove cache dir: {cache_dir}"


def test_refresh(runpy):
    """
    Test esm when the "SUBPARSER" is refresh, it should update the cache
    """
    cache_dir = pathlib.Path(tempfile.mkdtemp())
    esm_cache = cache_dir / "cache" / "esm" / "local" / "test.msgpack"
    assert not esm_cache.exists()

    try:
        cmd = [
            sys.executable,
            str(runpy),
            "refresh",
            "test",
            f"--cache-dir={cache_dir / 'cache'}",
            f"--root-dir={cache_dir}",
            "--log-level=debug",
            "--run-name=test",
        ]
        ret = subprocess.run(cmd, capture_output=True, encoding="utf-8")
        assert "Changes were made by refresh" in ret.stderr

        assert esm_cache.exists()

        with esm_cache.open("rb") as fh:
            data = msgpack.load(fh)
            assert data == {
                "test_|-Description of test.succeed_with_changes_|-succeed_with_changes_|-": {
                    "testing": {
                        "new": "Something pretended to change",
                        "old": "Unchanged",
                    },
                    "tests": [[{"new": "new_test"}]],
                },
                "test_|-Description of test.update_|-update_|-": {"param_1": None},
            }

    finally:
        shutil.rmtree(cache_dir, ignore_errors=True)
        assert not cache_dir.exists(), f"Could not remove cache dir: {cache_dir}"


def test_ignore_changes(runpy):
    """
    When the `ignore_changes` requisite is given in sls file, Idem should update those parameter values to None so that
    present() will ignore updating those parameters.
    """
    cache_dir = pathlib.Path(tempfile.mkdtemp())
    esm_cache = cache_dir / "cache" / "esm" / "local" / "test.msgpack"
    assert not esm_cache.exists()

    try:
        # Run the state to create an esm cache
        with tempfile.NamedTemporaryFile("w", suffix=".sls", delete=True) as fh:
            fh.write(IGNORE_CHANGES_STATE_CREATE)
            fh.flush()

            cmd = [
                sys.executable,
                str(runpy),
                "state",
                fh.name,
                "--log-level=debug",
                f"--cache-dir={cache_dir / 'cache'}",
                f"--root-dir={cache_dir}",
                "--log-level=debug",
                "--run-name=test",
            ]
            ret = subprocess.run(cmd, capture_output=True, encoding="utf-8")

        assert ret.returncode == 0, ret.stderr
        assert esm_cache.exists()

        with esm_cache.open("rb") as fh:
            data = msgpack.load(fh)
            assert data == {
                "test_|-passing_state_|-my-state_|-": {
                    "param_1": {"a": "b"},
                    "param_2": {"a": {"b": "c"}, "b": "c"},
                }
            }

        # ignore_changes contains param_1 and param_2, param_1 will be updated since it is a require parameter.
        # param_2 will be ignored on update.
        with tempfile.NamedTemporaryFile("w", suffix=".sls", delete=True) as fh:
            fh.write(IGNORE_CHANGES_STATE_UPDATE)
            fh.flush()

            cmd = [
                sys.executable,
                str(runpy),
                "state",
                fh.name,
                "--log-level=debug",
                f"--cache-dir={cache_dir / 'cache'}",
                f"--root-dir={cache_dir}",
                "--log-level=debug",
                "--run-name=test",
            ]
            ret = subprocess.run(cmd, capture_output=True, encoding="utf-8")

        assert ret.returncode == 0, ret.stderr
        assert esm_cache.exists()

        with esm_cache.open("rb") as fh:
            data = msgpack.load(fh)
            assert data == {
                "test_|-passing_state_|-my-state_|-": {
                    "param_1": {"a": "c"},
                    "param_2": {"a": {"b": "c"}, "b": "c"},
                }
            }

            # ignore_changes contains a parameter path: param_1:a:b. Idem will go through the path and
            # assign {param_1: {a: {b: None}}}
            with tempfile.NamedTemporaryFile("w+", suffix=".sls", delete=True) as fh:
                fh.write(IGNORE_CHANGES_STATE_UPDATE_2)
                fh.flush()

                cmd = [
                    sys.executable,
                    str(runpy),
                    "state",
                    fh.name,
                    "--log-level=debug",
                    f"--cache-dir={cache_dir / 'cache'}",
                    f"--root-dir={cache_dir}",
                    "--log-level=debug",
                    "--run-name=test",
                ]
                ret = subprocess.run(cmd, capture_output=True, encoding="utf-8")

            assert ret.returncode == 0, ret.stderr
            assert esm_cache.exists()

            with esm_cache.open("rb") as fh:
                data = msgpack.load(fh)
                assert data == {
                    "test_|-passing_state_|-my-state_|-": {
                        "param_1": {"a": "b"},
                        "param_2": {"a": {"b": None}, "b": "d"},
                    }
                }
    finally:
        shutil.rmtree(cache_dir, ignore_errors=True)
        assert not cache_dir.exists(), f"Could not remove cache dir: {cache_dir}"

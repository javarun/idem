import json
import subprocess
import sys


def test_multiple_sls(runpy, tests_dir, mock_time):
    cmd = [
        sys.executable,
        runpy,
        "state",
        tests_dir / "sls" / "noop.sls",
        tests_dir / "sls" / "noop2.sls",
        "--output=json",
    ]
    ret = subprocess.run(cmd, capture_output=True, encoding="utf-8", env={})
    assert ret.returncode == 0, ret.stderr
    output = json.loads(ret.stdout)

    output["test_|-First State_|-First State_|-nop"].pop("start_time")
    output["test_|-First State_|-First State_|-nop"].pop("total_seconds")
    assert output["test_|-First State_|-First State_|-nop"] == {
        "__run_num": 1,
        "changes": {},
        "comment": "Success!",
        "esm_tag": "test_|-First " "State_|-First State_|-",
        "name": "First State",
        "new_state": None,
        "old_state": None,
        "result": True,
        "sls_meta": {"ID_DECS": {}, "SLS": {}},
        "tag": "test_|-First State_|-First " "State_|-nop",
    }
    output["test_|-Second State_|-Second State_|-nop"].pop("start_time")
    output["test_|-Second State_|-Second State_|-nop"].pop("total_seconds")
    assert output["test_|-Second State_|-Second State_|-nop"] == {
        "__run_num": 2,
        "changes": {},
        "comment": "Success!",
        "esm_tag": "test_|-Second " "State_|-Second State_|-",
        "name": "Second State",
        "new_state": None,
        "old_state": None,
        "result": True,
        "sls_meta": {"ID_DECS": {}, "SLS": {}},
        "tag": "test_|-Second State_|-Second " "State_|-nop",
    }

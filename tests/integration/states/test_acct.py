from pytest_idem.runner import run_sls
from pytest_idem.runner import run_yaml_block
from pytest_idem.runner import tpath_hub


def test_basic():
    """
    Add a new profile to the dictionary under the "test" provider.
    Verify that its kwargs constitute the new profile
    """
    state = """
    new_profile:
      acct.profile:
        - provider_name: test
        - key_1: value_1
        - key_2: value_2

    test_result:
      test.acct:
        - acct_profile: new_profile
        - require:
          - acct: new_profile
    """
    ret = run_yaml_block(state)
    assert ret["acct_|-new_profile_|-new_profile_|-profile"]["changes"] == {
        "new": {"profiles": ["new_profile"]},
        "old": {"profiles": []},
    }
    assert ret["test_|-test_result_|-test_result_|-acct"]["new_state"] == {
        "key_1": "value_1",
        "key_2": "value_2",
    }


def test_duplicate():
    """
    Verify that adding a duplicate profile results in a failure
    """
    state = """
    new_profile:
      acct.profile:
        - provider_name: test

    duplicate_profile:
      acct.profile:
        - profile_name: new_profile
        - provider_name: test
        - require:
          - acct: new_profile
    """
    ret = run_yaml_block(state)
    assert (
        "Overwriting 'new_profile' under provider 'test'"
        in ret["acct_|-duplicate_profile_|-duplicate_profile_|-profile"]["comment"]
    )


def test_processed():
    with tpath_hub() as hub:
        ret = run_sls(["acct_mod_1"], hub=hub)

    # Verify that a provider name of "test.mod" gets modified by the tpath acct plugin
    assert (
        ret["acct_|-processed_profile_|-processed_profile_|-profile"]["result"] is True
    )
    assert ret["test_|-processed_result_|-processed_result_|-acct"]["new_state"] == {
        "key_1": "value_1",
        "modded": True,
    }


def test_unprocessed():
    with tpath_hub() as hub:
        ret = run_sls(["acct_mod_2"], hub=hub)

    # Verify that a provider name of "test" is not modified by the tpath acct plugin
    assert (
        ret["acct_|-unprocessed_profile_|-unprocessed_profile_|-profile"]["result"]
        is True
    )
    assert ret["test_|-unprocessed_result_|-unprocessed_result_|-acct"][
        "new_state"
    ] == {"key_1": "value_1"}


def test_acct_data():
    """
    Verify that a one-off profile works
    """
    state = """
    test_result:
      test.acct:
        - acct_profile: new_profile
        - acct_data:
            profiles:
              test:
                new_profile:
                  key_1: value_1
                  key_2: value_2
    """
    ret = run_yaml_block(state)
    assert ret["test_|-test_result_|-test_result_|-acct"]["new_state"] == {
        "key_1": "value_1",
        "key_2": "value_2",
    }


def test_arg_bind():
    state = """
    mock_acct:
      test.present:
        - new_state:
            key_1: value_1
            key_2: value_2

    new_profile:
      acct.profile:
        - provider_name: test
        - key_1: ${test:mock_acct:key_1}
        - key_2: ${test:mock_acct:key_2}

    test_result:
      test.acct:
        - acct_profile: new_profile
        - require:
          - acct: new_profile
    """
    ret = run_yaml_block(state)

    # Verify that the present state ran correctly
    assert ret["test_|-mock_acct_|-mock_acct_|-present"]["new_state"] == {
        "key_1": "value_1",
        "key_2": "value_2",
    }

    # Verify that the argbinding to the new profile was successful
    assert ret["acct_|-new_profile_|-new_profile_|-profile"]["changes"] == {
        "new": {"profiles": ["new_profile"]},
        "old": {"profiles": []},
    }

    # Verify that the credentials profile is available in the new state
    assert ret["test_|-test_result_|-test_result_|-acct"]["new_state"] == {
        "key_1": "value_1",
        "key_2": "value_2",
    }


def test_copy_from_new_acct_data():
    state = """
    new_profile:
      acct.profile:
        - provider_name: test
        - acct_data: {"profiles": {"test": {"source": {"key_1": "overwritten", "key_3": "copied"}}}}
        - source_profile: source
        - key_1: value_1
        - key_2: value_2

    test_result:
      test.acct:
        - acct_profile: new_profile
        - require:
          - acct: new_profile
    """
    ret = run_yaml_block(state)
    assert ret["acct_|-new_profile_|-new_profile_|-profile"]["changes"] == {
        "new": {"profiles": ["new_profile"]},
        "old": {"profiles": []},
    }
    assert ret["test_|-test_result_|-test_result_|-acct"]["new_state"] == {
        "key_1": "value_1",
        "key_2": "value_2",
        "key_3": "copied",
    }


def test_copy_from_existing_acct_data():
    state = """
    new_profile:
      acct.profile:
        - provider_name: test
        - source_profile: source
        - key_1: value_1
        - key_2: value_2

    test_result:
      test.acct:
        - acct_profile: new_profile
        - require:
          - acct: new_profile
    """
    acct_data = {
        "profiles": {"test": {"source": {"key_1": "overwritten", "key_3": "copied"}}}
    }
    ret = run_yaml_block(state, acct_data=acct_data)
    assert ret["acct_|-new_profile_|-new_profile_|-profile"]["changes"] == {
        "new": {"profiles": ["source", "new_profile"]},
        "old": {"profiles": ["source"]},
    }
    assert ret["test_|-test_result_|-test_result_|-acct"]["new_state"] == {
        "key_1": "value_1",
        "key_2": "value_2",
        "key_3": "copied",
    }

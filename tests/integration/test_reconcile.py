import tempfile

import dict_tools.differ as differ
import pop as pop
import pytest


@pytest.mark.asyncio
async def test_no_reconcile(hub, code_dir):
    reconciler = "none"

    ret = await hub.reconcile.init.run(
        plugin=reconciler,
        pending_plugin="default",
        name="test",
        apply_kwargs=dict(
            sls_sources=[f"file://{code_dir}/tests/sls"],
            render="yaml",
            runtime="serial",
            cache_dir=tempfile.mkdtemp(),
            sls="hello.sls",
            test=False,
            acct_file=None,
            acct_key=None,
            acct_profile="",
            managed_state={},
        ),
    )

    assert ret
    assert ret["re_runs_count"] == 0
    assert ret["require_re_run"] is False


@pytest.mark.asyncio
async def test_basic_reconcile_no_reruns(hub, mock_hub, code_dir):
    reconciler = "basic"
    # Set up the hub like idem does
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")

    mock_hub.reconcile.basic.loop = hub.reconcile.basic.loop

    hub.idem.RUNS = {
        "test": {
            "running": {
                "test.succeed_without_changes_|-state name_|-state name_|-name": {
                    "changes": {},
                    "comment": "The named state test.succeed_without_changes is not available",
                    "name": "state.name",
                    "result": True,
                }
            }
        }
    }

    ret = await hub.reconcile.init.run(
        plugin=reconciler,
        pending_plugin="default",
        name="test",
        apply_kwargs=dict(
            sls_sources=[f"file://{code_dir}/tests/sls"],
            render="yaml",
            runtime="serial",
            cache_dir=tempfile.mkdtemp(),
            sls="hello.sls",
            test=False,
            acct_file=None,
            acct_key=None,
            acct_profile="",
            managed_state={},
        ),
    )

    assert ret
    assert ret["re_runs_count"] == 0
    assert ret["require_re_run"] is False


@pytest.mark.asyncio
async def test_basic_reconcile_reruns(hub, mock_hub, code_dir):
    # Test reruns. One state require reconcile, the other does not.
    reconciler = "basic"
    sls_sources = [f"file://{code_dir}/tests/sls"]
    cache_dir = tempfile.mkdtemp()

    mock_hub.reconcile.init.run = hub.reconcile.init.run
    mock_hub.reconcile.basic.loop = hub.reconcile.basic.loop
    mock_hub.reconcile.basic.has_passed = hub.reconcile.basic.has_passed
    mock_hub.reconcile.wait.static.get = hub.reconcile.wait.static.get
    mock_hub.reconcile.pending.default.is_pending = (
        hub.reconcile.pending.default.is_pending
    )
    mock_hub.idem.RUNS = {
        "test": {
            "running": {
                "test.succeed_without_changes_|-state name1_|-state name1_|-name1": {
                    "new_state": None,
                    "old_state": None,
                    "changes": {"change": "is constant"},
                    "comment": "The named state test.succeed_without_changes is not available",
                    "name": "state.name1",
                    "result": True,
                },
                "test.succeed_without_changes_|-state name2_|-state name2_|-name2": {
                    "new_state": None,
                    "old_state": None,
                    "changes": {},
                    "comment": "No changes. No reconcile",
                    "name": "state.name2",
                    "result": True,
                },
            }
        }
    }

    def _check_run_init_start_params(name, pending_tags):
        assert name == "test"
        assert pending_tags
        assert (
            "test.succeed_without_changes_|-state name1_|-state name1_|-name1"
            in pending_tags
        )
        assert len(pending_tags) == 1

    mock_hub.idem.run.init.start.side_effect = _check_run_init_start_params
    ret = await mock_hub.reconcile.init.run(
        plugin=reconciler,
        pending_plugin="default",
        name="test",
        apply_kwargs=dict(
            sls_sources=sls_sources,
            render="yaml",
            runtime="serial",
            cache_dir=cache_dir,
            sls="hello.sls",
            test=False,
            acct_file=None,
            acct_key=None,
            acct_profile="",
            acct_blob=None,
            subs=[],
            managed_state={},
        ),
    )

    assert ret
    assert ret["re_runs_count"] == 3
    assert ret["require_re_run"] is True


@pytest.mark.asyncio
async def test_reconcile_rerun_accumulative_changes(hub, mock_hub, code_dir):
    # this test verifies that the reconciliation loop updates the 'old_state'
    # and 'changes' of the last run before it returns
    reconciler = "basic"
    sls_sources = [f"file://{code_dir}/tests/sls"]
    cache_dir = tempfile.mkdtemp()

    mock_hub.reconcile.init.run = hub.reconcile.init.run
    mock_hub.reconcile.basic.loop = hub.reconcile.basic.loop
    mock_hub.reconcile.basic.has_passed = hub.reconcile.basic.has_passed
    mock_hub.reconcile.wait.static.get = hub.reconcile.wait.static.get
    mock_hub.reconcile.pending.default.is_pending = (
        hub.reconcile.pending.default.is_pending
    )
    mock_hub.reconcile.basic.update_changes = hub.reconcile.basic.update_changes
    mock_hub.idem.RUNS = {
        "test": {
            "running": {
                "test.succeed_without_changes_|-state name_A_|-state name_|-name": {
                    "old_state": {"hello": "A", "world": "A"},
                    "new_state": {"hello": "A1", "world": "A1"},
                    "changes": {"change": "is constant"},
                    "comment": "The named state test.succeed_without_changes is not available",
                    "name": "state.name",
                    "result": True,
                },
                "test.succeed_without_changes_|-state name_B_|-state name_|-name": {
                    "old_state": {"hello": "B", "world": "B"},
                    "new_state": {"hello": "B1", "world": "B1"},
                    "changes": {"change": "is constant"},
                    "comment": "The named state test.succeed_without_changes is not available",
                    "name": "state.name",
                    "result": True,
                },
            }
        }
    }

    def _check_run_init_start_params(name, pending_tags):
        assert name == "test"
        assert pending_tags
        assert len(pending_tags) == 2

    mock_hub.idem.run.init.start.side_effect = _check_run_init_start_params
    ret = await mock_hub.reconcile.init.run(
        plugin=reconciler,
        pending_plugin="default",
        name="test",
        apply_kwargs=dict(
            subs=["states"],
            sls_sources=sls_sources,
            render="yaml",
            runtime="serial",
            cache_dir=cache_dir,
            sls="hello.sls",
            test=False,
            acct_file=None,
            acct_key=None,
            acct_blob=None,
            acct_profile="",
            managed_state={},
        ),
    )

    assert ret
    assert ret["re_runs_count"] == 3
    assert ret["require_re_run"] is True
    run_state_A = mock_hub.idem.RUNS["test"]["running"][
        "test.succeed_without_changes_|-state name_A_|-state name_|-name"
    ]
    run_state_B = mock_hub.idem.RUNS["test"]["running"][
        "test.succeed_without_changes_|-state name_B_|-state name_|-name"
    ]
    assert run_state_A
    assert run_state_A["changes"]
    assert run_state_B["changes"]
    assert run_state_A["changes"] == differ.deep_diff(
        run_state_A["old_state"], run_state_A["new_state"]
    )
    assert run_state_B["changes"] == differ.deep_diff(
        run_state_B["old_state"], run_state_B["new_state"]
    )

processed_profile:
  acct.profile:
    - provider_name: test.mod
    - key_1: value_1

processed_result:
  test.acct:
    - acct_profile: processed_profile
    - require:
        - acct: processed_profile

State {{ params.get('state').get('name') }} Present:
  nest.test.succeed_with_kwargs_as_changes:
    - name: {{ params.get('state').get('name') }}
    - parameters:
        location: {{ params['locations'][0] }}
        backup_location: {{ params['locations'][1] }}
        empty: {{ params.get('empty') }}
        empty_str: {{ params.get('empty') }}_str
        include_1: {{ params.get('include_one') }}
        include_2: {{ params.get('include_two') }}

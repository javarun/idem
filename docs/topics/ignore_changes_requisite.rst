===========================
ignore_changes Requisite
===========================

ignore_changes can be used in sls blocks to prevented parameters to be updated on brown-field resources.
If a parameter is specified under the ignore_changes and this parameter will be overridden with None and present() function will ignore updating such None-value parameters.

In the following example, State_A is a green-field resource. During the first Idem run, Idem will create State_A resource with
the tags value. However, since ignore_changes contains "tags", during the second Idem run to update the State_A resource, Idem
will not update tags even when the tags of the resource has been deviated away from the initial {tag-key: tag-value} value.

.. code-block:: sls

   State_A:
      cloud.instance.present:
        - name: my-resource
        - tags: {tag-key: tag-value}
        - ignore_changes:
          - tags

Note: ignore_changes requisite only takes into effect on a brown-field resource. That is, the enforced State_A exists in ESM cache or the resource_id has been supplied in sls file.

========
Idem 7.1
========

Idem 7.1 extends the capabilities of Idem 7 by allowing the acct system
to be used by `idem exec` instead of just `idem state`. This also makes
it possible to pass in a `ctx` option to exec functions, like state
functions.
